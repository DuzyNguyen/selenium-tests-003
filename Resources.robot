*** Settings ***
Documentation    Suite description
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Library           Selenium2Library
Library           Collections
Library           OperatingSystem
Library           String
#Library           HttpLibrary.HTTP

*** Variables ***

*** Keywords ***

Launch Browser
    open browser  about:blank  chrome
#    maximize browser window

Exit Browser
    close browser

Open Browser Stack
    [Arguments]   ${BROWSER}  ${BROWSER_VERSION}  ${OS}  ${OS_VERSION}
    Open Browser   url=${SiteUrl}   browser=${BROWSER}   remote_url=${RemoteURL}   desired_capabilities=browser:${BROWSER},browser_version:${BROWSER_VERSION},os:${OS},os_version:${OS_VERSION}
    maximize browser window

Log my multiple variables
    Log  @{MY_LIST_VARIABLE} [0]
    Log  @{MY_LIST_VARIABLE} [1]
    Log  @{MY_LIST_VARIABLE} [2]

Set a variable in the test case
    ${my_new_variable} =  set variable  This is a new variable

Start browser
    [arguments]  ${url}  ${timeout}  ${delay}  ${t_name}=generic
    ${browser}=  OperatingSystem.Get Environment Variable  browser  ${Browser}
    ${browser_version}=  OperatingSystem.Get Environment Variable  browser_version  ${EMPTY}
    ${os}=  OperatingSystem.Get Environment Variable  os  ${EMPTY}
    ${os_version}=  OperatingSystem.Get Environment Variable  os_version  ${EMPTY}
    ${is_browserstack}=  OperatingSystem.Get Environment Variable  browserstack  false
    Run Keyword If	'${is_browserstack}' == 'true' 	Start browser in Browserstack  TEST_NAME=${t_name}  BROWSER=${browser}  BROWSER VERSION=${browser_version}  OS=${os}  OS VERSION=${os_version}   url=${url}  timeout=${timeout}  delay=${delay}
    Run Keyword If	'${is_browserstack}' == 'false'	Start browser locally  browser=${browser}  url=${url}  timeout=${timeout}  delay=${delay}
    maximize browser window

Click on element by css
    [arguments]  ${ELEMENT_IDENTIFIER}
    Wait Until Element Is Visible  css=${ELEMENT_IDENTIFIER}
    click element  css=${ELEMENT_IDENTIFIER}

Click on element by id
    [arguments]  ${ELEMENT_IDENTIFIER}
    Wait Until Element Is Visible  id=${ELEMENT_IDENTIFIER}
    click element  id=${ELEMENT_IDENTIFIER}

Click on element by class
    [arguments]  ${ELEMENT_IDENTIFIER}
    Wait Until Element Is Visible  class=${ELEMENT_IDENTIFIER}
    click element  class=${ELEMENT_IDENTIFIER}

Start browser locally
    [arguments]  ${browser}  ${url}  ${timeout}  ${delay}
    open browser  ${url}  ${browser}
    Set Selenium Timeout  ${timeout}
    Set Selenium Speed    ${delay}

Click on element by name
    [arguments]  ${ELEMENT_IDENTIFIER}
    Wait Until Element Is Visible  name=${ELEMENT_IDENTIFIER}
    click element  name=${ELEMENT_IDENTIFIER}

Click on element by link
    [arguments]  ${ELEMENT_IDENTIFIER}
    Wait Until Element Is Visible  link=${ELEMENT_IDENTIFIER}
    click element  link=${ELEMENT_IDENTIFIER}

Click on element by xpath
    [arguments]  ${ELEMENT_IDENTIFIER}
    Wait Until Element Is Visible  xpath=${ELEMENT_IDENTIFIER}
    click element  xpath=${ELEMENT_IDENTIFIER}

Enter text by id
    [arguments]  ${ELEMENT_IDENTIFIER}  ${INPUT_TEXT}
    wait until element is visible  id=${ELEMENT_IDENTIFIER}
    focus  id=${ELEMENT_IDENTIFIER}
    clear element text  id=${ELEMENT_IDENTIFIER}
    input text  id=${ELEMENT_IDENTIFIER}  ${INPUT_TEXT}

Enter text by css
    [arguments]  ${ELEMENT_IDENTIFIER}  ${INPUT_TEXT}
    wait until element is visible  css=${ELEMENT_IDENTIFIER}
    focus  css=${ELEMENT_IDENTIFIER}
    clear element text  css=${ELEMENT_IDENTIFIER}
    input text  css=${ELEMENT_IDENTIFIER}  ${INPUT_TEXT}

Enter text by name
    [arguments]  ${ELEMENT_IDENTIFIER}  ${INPUT_TEXT}
    wait until element is visible  name=${ELEMENT_IDENTIFIER}
    focus  name=${ELEMENT_IDENTIFIER}
    clear element text  name=${ELEMENT_IDENTIFIER}
    input text  name=${ELEMENT_IDENTIFIER}  ${INPUT_TEXT}

Get data
    ${jsonfile}    Get File    /Users/sandeepreddy/Downloads/UI Automation/data/data.json
    ${json}=    evaluate    json.loads('''${jsonfile}''')    json
    set global variable  ${data}  ${json}
    [return]  ${json}


