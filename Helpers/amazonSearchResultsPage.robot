*** Settings ***
Documentation    Suite description
Resource  ../Resources.robot

*** Variables ***

${SEARCHWORD}  results for "naruto"
${SEARCHRESULTS}  Japanime Naruto Shippuden The Board Game

*** Keywords ***

Confirm search results
    Wait Until Page Contains  ${SEARCHWORD}
    wait until page contains  ${SEARCHRESULTS}

#Click on element by id