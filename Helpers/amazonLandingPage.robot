*** Settings ***
Documentation    Suite description
Resource  ../Resources.robot

*** Variables ***

${SEARCHBOX}  twotabsearchtextbox
${SEARCHTEXT}  naruto
${SEARCHBUTTON}  \#nav-search > form > div.nav-right > div > input

*** Keywords ***

Navigate to page
    go to  https://www.amazon.com.au
    wait until element is enabled  id=${SEARCHBOX}

Enter search text
    Enter text by id  ${SEARCHBOX}  ${SEARCHTEXT}
#    click on element by id  ${SEARCHBUTTON}
    click on element by css  ${SEARCHBUTTON}

