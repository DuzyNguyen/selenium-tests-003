*** Settings ***
Documentation    Suite description
Resource  ../Resources.robot
Resource  ../Helpers/amazonLandingPage.robot
Resource  ../Helpers/amazonSearchResultsPage.robot


Suite Setup  Launch Browser
Suite Teardown  Exit Browser

*** Test Cases ***

Test01 - Navigate to Amazon and search for product
    Navigate to page
    Enter search text

Test02 - Confirm search results and make selection
    Confirm search results



*** Keywords ***
